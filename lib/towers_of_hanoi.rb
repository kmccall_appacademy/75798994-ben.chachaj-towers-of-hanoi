# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  # * play
  # * render
  # * won?
  def play
    puts render
    user_turn until won?
  end

  def render

    level3 = towers.map {|tower| tower.length >= 3 ? tower_print(tower[2]) : "     "}
    level2 = towers.map {|tower| tower.length >= 2 ? tower_print(tower[1]) : "     "}
    level1 = towers.map {|tower| tower.length >= 1 ? tower_print(tower[0]) : "     "}

    "#{level3.join(" ")}\n#{level2.join(" ")}\n#{level1.join(" ")}\n_____ _____ _____ "
  end


  def tower_print(input)
    if input == 3
      "|---|"
    elsif input == 2
      " |-| "
    elsif input == 1
      "  |  "
    end
  end

  def move(from_tower, to_tower)
    from = towers[from_tower]
    to = towers[to_tower]
    disc = from.pop
    to.push(disc)
    puts render
  end

  def user_turn
    print "Select \"to\" and \"from\" with a space inbetween (e.g. 0 1): "
    input = gets.chomp
    from, to = input.split(" ").map(&:to_i)
    until valid_move?(from, to)
      from, to = valid_input
    end
    move(from, to)
  end

  def valid_input
    print "Input incorrect, try again:"
    input = gets.chomp
    input.split(" ").map(&:to_i)
  end

  def valid_move?(from_tower, to_tower)
    fr = towers[from_tower]
    to = towers[to_tower]
    return false if fr.empty?
    return false if !to.empty? && fr.last > to.last
    true
  end

  def won?
    if towers[2].length == 3 || towers[1].length == 3
      puts "Mission Completed"
      puts render
      true
    else
      false
    end
  end



end


if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  # puts t.render
  # t.move(0,2)
  # puts t.render
  t.play
end
